import { https } from "./configURL";

export const movieService = {
  getMovieList: () => {
    return https.get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03");
  },
  getMovieListByTheatre: () => {
    return https.get(
      "api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03"
    );
  },
  getMovieBanner: () => {
    return https.get("api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovieInfo: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  getMovieSchedule: (id) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
  },
};
