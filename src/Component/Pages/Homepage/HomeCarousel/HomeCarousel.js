import React from 'react'
import { Desktop, Tablet } from '../../../../HOC/Responsive';
import HomeCarouselDesktop from './HomeCarouselDesktop';
import HomeCarouselTablet from './HomeCarouselTablet';

export default function HomeCarousel({bannerArr}) {
  
  return (
    <div>
      <Desktop><HomeCarouselDesktop bannerArr={bannerArr}></HomeCarouselDesktop></Desktop>
      <Tablet><HomeCarouselTablet bannerArr={bannerArr}></HomeCarouselTablet></Tablet>
    </div>
  )
}
