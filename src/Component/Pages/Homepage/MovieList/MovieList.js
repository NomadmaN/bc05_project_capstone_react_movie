import React from 'react'
import { Desktop, Tablet, Mobile } from '../../../../HOC/Responsive';
import MovieListDesktop from './MovieListDesktop';
import MovieListTablet from './MovieListTablet';
import MovieListMobile from './MovieListMobile';


export default function MovieList({movieArr}) {
    
  return (
    <> 
        <div>
            <Desktop><MovieListDesktop movieArr={movieArr}></MovieListDesktop></Desktop>
            <Tablet><MovieListTablet movieArr={movieArr}></MovieListTablet></Tablet>
            <Mobile><MovieListMobile movieArr={movieArr}></MovieListMobile></Mobile>
        </div>
    </>
  )
}
