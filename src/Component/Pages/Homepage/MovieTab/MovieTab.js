import { Tabs } from 'antd';
import React, { useEffect, useState } from 'react'
import { movieService } from '../../../../services/movieService';
import MovieTabItem from './MovieTabItem';

export default function MovieTab() {
  const [movieTheatreArr, setMovieTheatreArr] = useState([]);
  const onChange = (key) => {
      console.log('MovieTab - maCumRap: ',key)
  }
  useEffect (() => {
      movieService.getMovieListByTheatre()
      .then((res) => {
        // console.log('getMovieListByTheatre: ',res);
        setMovieTheatreArr(res.data.content)
      })
      .catch((err) => {
        console.log(err);
      });
  },[])
  let renderTheatreList = () => {
      return movieTheatreArr.map((heThongRap, index) => {
          return {
              label: <img width={50} src={heThongRap.logo} alt=''></img>,
              key: heThongRap.maHeThongRap,
              children: (
                  <Tabs key={index}
                      tabPosition='left'
                      defaultActiveKey="1"
                      onChange={onChange}
                      items={heThongRap.lstCumRap.map((cumRap) => {
                          return {
                              label: (
                                  <div>
                                      {' '}
                                      <p>{cumRap.tenCumRap}</p>
                                  </div>
                                  ),
                              key: cumRap.maCumRap,
                              children: cumRap.danhSachPhim.slice(0,10).map((phim) => {
                                  return (
                                      <MovieTabItem movie={phim}></MovieTabItem>
                                  )
                              }),
                          }
                      })}
                  />
              ),
          };
      });
  };
return (
  <div className='hidden lg:block py-5'>
      <Tabs
          tabPosition='left'
          defaultActiveKey="1"
          onChange={onChange}
          items={renderTheatreList()}
      />
  </div>
)
}
