import React from 'react'
import { Desktop, Tablet, Mobile } from '../../../../HOC/Responsive';
import CinemaTabDesktop from './CinemaTabDesktop';
import CinemaTabTablet from './CinemaTabTablet';
import CinemaTabMobile from './CinemaTabMobile';

export default function CinemaTab({id}) {
  return (
    <div>
        <Desktop><CinemaTabDesktop id={id}/></Desktop>
        <Tablet><CinemaTabTablet id={id}/></Tablet>
        <Mobile><CinemaTabMobile id={id}/></Mobile>
    </div>
  )
}
