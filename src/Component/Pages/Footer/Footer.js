import React, { useEffect, useState } from 'react'
import { Desktop, Mobile, Tablet } from '../../../HOC/Responsive'
import { movieService } from '../../../services/movieService';
import FooterDesktop from './FooterDesktop'
import FooterMobile from './FooterMobile';
import FooterTablet from './FooterTablet';

export default function Footer() {
  const [theatreArr, setTheatreArr] = useState([]);
  useEffect (() => {
      movieService.getMovieListByTheatre()
      .then((res) => {
        setTheatreArr(res.data.content)
      })
      .catch((err) => {
        console.log(err);
      });
  },[]);
  
  return (
    <div>
        <Desktop><FooterDesktop theatreArr={theatreArr}></FooterDesktop></Desktop>
        <Tablet><FooterTablet theatreArr={theatreArr}></FooterTablet></Tablet>
        <Mobile><FooterMobile theatreArr={theatreArr}></FooterMobile></Mobile>
    </div>
  )
}
