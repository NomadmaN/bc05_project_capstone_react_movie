import React from 'react'
import Header from '../Component/Pages/Header/Header'

export default function Layout({children}) {
  return (
    <>
      <div>
        {/* Header alawys on top */}
        <Header></Header>
        {/* Between are components, set by children */}
      </div>
      <div style={{paddingTop: '80px'}}>
        {children}
      </div>
    </>
  )
}
