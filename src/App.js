import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from './HOC/Layout';
import HomePage from './Component/Pages/Homepage/HomePage';
import LoginPage from './Component/Pages/LoginPage/LoginPage';
import SignupPage from './Component/Pages/SignupPage/SignupPage';
import DetailPage from './Component/Pages/DetailPage/DetailPage';
import AdminPage from './Component/Pages/AdminPage/AdminPage';
import NotFoundPage from './Component/Pages/NotFoundPage/NotFoundPage';
import Loading from './Component/Pages/Loading/Loading';

function App() {
  return (
    <div>
      <Loading></Loading>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout><HomePage></HomePage></Layout>}></Route>
          <Route path='/login' element={<Layout><LoginPage></LoginPage></Layout>}></Route>
          <Route path='/signup' element={<Layout><SignupPage></SignupPage></Layout>}></Route>
          <Route path='/detail/:id' element={<Layout><DetailPage></DetailPage></Layout>}></Route>
          <Route path='/admin/user' element={<Layout><AdminPage></AdminPage></Layout>}></Route>
          <Route path='*' element={<NotFoundPage></NotFoundPage>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
